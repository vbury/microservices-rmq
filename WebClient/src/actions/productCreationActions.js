import fetch from 'isomorphic-fetch';

import conf from '../config/conf.json';
import { browserHistory } from 'react-router';

import { logout } from './';

export const PRODUCT_CREATION_SUCCESS = 'success product creation';

function productCreationSuccess(link) {
  return {
    type: PRODUCT_CREATION_SUCCESS,
    link,
  };
}

export const PRODUCT_CREATION_FAILURE = 'error creating product';

function productCreationFailure(error) {
  return {
    type: PRODUCT_CREATION_FAILURE,
    error,
  };
}

export function createProduct(params, token) {
  return function (dispatch) {
    return fetch(`${conf.uri}products/`, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        'x-access-token': token,
      }),
      body: JSON.stringify(params),
    }).then(response => response.json()).then((message) => {
      if (message.message === 'Failed to Authentication token.' || message.message === 'This route can only be accessed by an administrator' || message.status === 401) {
        dispatch(logout());
        browserHistory.push('/login');
      } else {
        if (message.error) {
          return dispatch(productCreationFailure(message.error));
        }
        return dispatch(productCreationSuccess(message));
      }
    });
  };
}
