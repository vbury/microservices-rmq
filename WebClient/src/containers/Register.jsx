import { connect } from 'react-redux';

import { register } from '../actions/authActions';
import _register from '../components/_register';
const mapStateToProps = ({ auth }) => ({
  auth,
});

const mapDispatchToProps = dispatch => ({
  register: (user) => {
    dispatch(register(user));
  },
});

const Register = connect(mapStateToProps,
	mapDispatchToProps)(_register);

export default Register;
