import { connect } from 'react-redux';

import { _productDetails } from '../components';

import { fetchById } from '../actions';

const mapStateToProps = ({ api, auth }) => {
  const { details } = api;
  const { role } = auth.user;
  return {
    details,
    role,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchProduct: (id) => {
    dispatch(fetchById(id));
  },
});

const ProductDetails = connect(
	mapStateToProps,
	mapDispatchToProps,
)(_productDetails);

export default ProductDetails;
