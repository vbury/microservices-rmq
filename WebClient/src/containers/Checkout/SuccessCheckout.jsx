import { connect } from 'react-redux';

import { _successCheckout } from '../../components';

import { getTransactionDetails, resetCart } from '../../actions';

const mapStateToProps = ({ auth, checkout }) => {
  const { token } = auth;
  const { _id } = auth.user;
  return {
    _id,
    token,
    checkout,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchDetails: (transactionId, uid,token) => {
    dispatch(getTransactionDetails(transactionId, uid,token));
    dispatch(resetCart());
  },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(_successCheckout);
