import com.rabbitmq.client.*;
import io.jsonwebtoken.*;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.util.Date;
import java.util.TreeMap;

/**
 * Created by anymaniax on 31/05/17.
 */
public class App {
    private static final String EXCHANGE_NAME = "auth";
    private static final String[] tags = {"auth", "requireId", "requireAdmin"};

    public static void main(String[] argv) throws Exception {
        String server = System.getenv("SERVER");
        ConnectionFactory factory = new ConnectionFactory();
        if(server != null){
            factory.setHost(server);
            factory.setPort(5672);
            System.out.println(server);
        } else {
            factory.setHost("localhost");
        }
        Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        String queueName = channel.queueDeclare().getQueue();
        for (String tag:tags) {
            channel.queueBind(queueName, EXCHANGE_NAME, tag);
        }
        channel.basicQos(1);

        System.out.println("Auth API up and running");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                channel.exchangeDeclare(properties.getReplyTo(), "direct");
                AMQP.BasicProperties props = new AMQP.BasicProperties()
                        .builder()
                        .correlationId(properties.getCorrelationId())
                        .build();
                if(envelope.getRoutingKey().equals("auth")){
                    System.out.println("tag => auth");

                    JSONObject mess = null;
                    JSONObject success = new JSONObject();
                    try {
                        mess = new JSONObject(message);
                        mess = new JSONObject(mess.getString("body"));
                        mess = new JSONObject(mess.getString("ByUser"));
                        final JSONObject address = new JSONObject(mess.getString("address"));

                        Date expire = new Date(System.currentTimeMillis() + 72 * 3600 * 1000);
                        String jwt =
                                Jwts.builder()
                                        .claim("_id",mess.getString("_id"))
                                        .claim("lastname",mess.getString("lastname"))
                                        .claim("firstname",mess.getString("firstname"))
                                        .claim("username",mess.getString("username"))
                                        .claim("email",mess.getString("email"))
                                        .claim("sex",mess.getString("sex"))
                                        .claim("phone",mess.getString("phone"))
                                        .claim("address", new TreeMap<String, Object>(){{
                                            put("street", address.getString("street"));
                                            put("number", address.getString("number"));
                                            put("town", address.getString("town"));
                                            put("postalCode", address.getString("postalCode"));
                                            put("country", address.getString("country"));
                                        }})
                                        .claim("role",mess.getString("role"))
                                        .setExpiration(expire)
                                        .signWith(SignatureAlgorithm.HS256,"DONALDTRUMP".getBytes("UTF-8"))
                                        .compact();

                        success.put("success",true);
                        success.put("message","Enjoy your token!");
                        success.put("token",jwt);
                        channel.basicPublish(properties.getReplyTo(), "auth", props, success.toString().getBytes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if(envelope.getRoutingKey().equals("requireId")) {
                    System.out.println("tag => requireId");
                    JSONObject mess = null;
                    JSONObject response = new JSONObject();
                    try {
                        mess = new JSONObject(message);
                        Claims claims = Jwts.parser()
                                .setSigningKey("DONALDTRUMP".getBytes("UTF-8"))
                                .parseClaimsJws(mess.getString("token")).getBody();
                        if((claims.get("role").equals("admin") || claims.get("_id").equals(mess.getString("id"))) && claims.getExpiration().after(new Date())){
                            response.put("success",true);
                            response.put("message","Welcome " + claims.get("username"));
                        } else {
                            response.put("success",false);
                            response.put("message","Failed to Authentication token.");
                        }
                        channel.basicPublish(properties.getReplyTo(), "requireId", props, response.toString().getBytes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if(envelope.getRoutingKey().equals("requireAdmin")){
                    System.out.println("tag => requireAdmin");
                    JSONObject mess = null;
                    JSONObject response = new JSONObject();
                    try {
                        mess = new JSONObject(message);
                        Claims claims = Jwts.parser()
                                .setSigningKey("DONALDTRUMP".getBytes("UTF-8"))
                                .parseClaimsJws(mess.getString("token")).getBody();
                        if(claims.get("role").equals("admin") && claims.getExpiration().after(new Date())){
                            response.put("success",true);
                            response.put("message","Welcome " + claims.get("username"));
                        } else {
                            response.put("success",false);
                            response.put("message","This route can only be accessed by an administrator");
                        }
                        channel.basicPublish(properties.getReplyTo(), "requireAdmin", props, response.toString().getBytes());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("An unexpect error happened");
                }
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}
