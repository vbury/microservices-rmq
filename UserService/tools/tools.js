import dev from '../config/dev.json';
import prod from '../config/prod.json';

const config = process.env.DEBUG == 0 ? prod : dev;

module.exports = {
  config,
};
