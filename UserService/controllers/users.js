import bcrypt from 'bcryptjs';
import {
    User,
    Salt,
} from '../models/db';

const getBuffer = object => Buffer.from(JSON.stringify(object));

const handleTags = {
  getByUandE: (channel, msg) => {
    console.log('tag => getByUandE');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const username = new RegExp(getUser.username, 'i');
    User.findOne({
      $or: [{
        username: {
          $regex: username,
        },
      }, {
        email: {
          $regex: username,
        },
      }],
    }, (err, user) => {
      if (err) {
        console.log(err);
        const error = {
          success: false,
          message: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getByUandE', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
      if (!user) {
        const error = {
          success: false,
          message: 'Authentication failed.',
        };
        channel.publish(msg.properties.replyTo, 'getByUandE', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (user) {
        user.comparePassword(getUser.password, (err2, isMatch) => {
          if (isMatch) {
            const ByUser = {
              _id: user.id,
              lastname: user.lastname,
              firstname: user.firstname,
              username: user.username,
              email: user.email,
              sex: user.sex,
              phone: user.phone,
              address: {
                street: user.address.street,
                number: user.address.number,
                town: user.address.town,
                postalCode: user.address.postalCode,
                country: user.address.country,
              },
              role: user.role,
            };
            const success = {
              success: true,
              message: 'Authentication success.',
              ByUser,
            };
            channel.publish(msg.properties.replyTo, 'getByUandE', getBuffer(success), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const error = {
              success: false,
              message: 'Authentication failed.',
            };
            channel.publish(msg.properties.replyTo, 'getByUandE', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      }
    });
    channel.ack(msg);
  },
  getAll: (channel, msg) => {
    console.log('tag => getAll');
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.find((err, users) => {
      if (!users) {
        const error = {
          success: false,
          err: 'No user found :(',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const allUsers = [];
        for (const user of users) {
          const ByUser = {
            _id: user.id,
            lastname: user.lastname,
            firstname: user.firstname,
            username: user.username,
            email: user.email,
            sex: user.sex,
            phone: user.phone,
            role: user.role,
            address: {
              street: user.address.street,
              number: user.address.number,
              town: user.address.town,
              postalCode: user.address.postalCode,
              country: user.address.country,
            },
          };
          allUsers.push(ByUser);
        }
        const success = {
          success: true,
          message: 'All users find with success.',
          allUsers,
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getById: (channel, msg) => {
    console.log('tag => getById');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.findById(getUser.id, (err, user) => {
      if (user) {
        const ByUser = {
          _id: user.id,
          lastname: user.lastname,
          firstname: user.firstname,
          username: user.username,
          email: user.email,
          sex: user.sex,
          phone: user.phone,
          address: {
            street: user.address.street,
            number: user.address.number,
            town: user.address.town,
            postalCode: user.address.postalCode,
            country: user.address.country,
          },
          role: user.role,
        };
        const success = {
          success: true,
          message: 'User find with success.',
          ByUser,
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const error = {
          success: false,
          message: 'No user found for that id.',
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  addUser: (channel, msg) => {
    console.log('tag => addUser');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.findOne({
      $or: [{
        username: getUser.body.username,
      }, {
        email: getUser.body.email,
      }],
    }, (err, user) => {
      if (user) {
        if (user.username === getUser.body.username) {
          const error = {
            success: false,
            error: 'Username already use.',
          };
          channel.publish(msg.properties.replyTo, 'addUser', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          const error = {
            success: false,
            error: 'Email already use.',
          };
          channel.publish(msg.properties.replyTo, 'addUser', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        }
      } else {
        const password = getUser.body.password;
        bcrypt.genSalt(Salt, (err2, salt) => {
          bcrypt.hash(password, salt, (err3, hash) => {
            getUser.body.password = hash;
            getUser.body.role = 'user';
            User(getUser.body).save((err4, ByUser) => {
              if (err4) {
                const error = {
                  success: false,
                  error: 'Could not create this user',
                };
                channel.publish(msg.properties.replyTo, 'addUser', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const success = {
                  success: true,
                  ByUser,
                };
                channel.publish(msg.properties.replyTo, 'addUser', getBuffer(success), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          });
        });
      }
    });
    channel.ack(msg);
  },
  delUser: (channel, msg) => {
    console.log('tag => delUser');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.findOneAndRemove({
      _id: getUser.id,
    }, (err) => {
      if (err) {
        const error = {
          success: false,
          error: 'Could not remove this user :(',
        };
        channel.publish(msg.properties.replyTo, 'delUser', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
        };
        channel.publish(msg.properties.replyTo, 'delUser', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  updateUser: (channel, msg) => {
    console.log('tag => updateUser');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.findOne({
      _id: getUser.id,
    }, (err, user) => {
      if (user.username === getUser.body.username && user.email === getUser.body.email) {
        User.update({
          _id: getUser.id,
        }, {
          $set: {
            lastname: getUser.body.lastname,
            firstname: getUser.body.firstname,
            address: {
              street: getUser.body.address.street,
              number: getUser.body.address.number,
              town: getUser.body.address.town,
              postalCode: getUser.body.address.postalCode,
              country: getUser.body.address.country,
            },
            sex: getUser.body.sex,
            phone: getUser.body.phone,
          },
        }, (err2, user2) => {
          if (err2) {
            const error = {
              success: false,
              error: 'Could not update this user',
            };
            channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const success = {
              success: true,
              message: 'User updated with success',
              user2,
            };
            channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(success), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      } else if (user.username != getUser.body.username && user.email == getUser.body.email) {
        User.findOne({
          username: getUser.body.username,
        }, (err2, user2) => {
          if (user2) {
            const error = {
              success: false,
              error: 'Username already use.',
            };
            channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            User.update({
              _id: getUser.id,
            }, {
              $set: {
                username: getUser.body.username,
                lastname: getUser.body.lastname,
                firstname: getUser.body.firstname,
                address: {
                  street: getUser.body.address.street,
                  number: getUser.body.address.number,
                  town: getUser.body.address.town,
                  postalCode: getUser.body.address.postalCode,
                  country: getUser.body.address.country,
                },
                sex: getUser.body.sex,
                phone: getUser.body.phone,
              },
            }, (err3, user3) => {
              if (err3) {
                const error = {
                  success: false,
                  error: 'Could not update this user',
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const success = {
                  success: true,
                  message: 'User updated with success',
                  user3,
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(success), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          }
        });
      } else if (user.email !== getUser.body.email && user.username === getUser.body.username) {
        User.findOne({
          email: getUser.body.email,
        }, (err2, user2) => {
          if (user2) {
            const error = {
              success: false,
              error: 'Email already use.',
            };
            channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            User.update({
              _id: getUser.id,
            }, {
              $set: {
                email: getUser.body.email,
                lastname: getUser.body.lastname,
                firstname: getUser.body.firstname,
                address: {
                  street: getUser.body.address.street,
                  number: getUser.body.address.number,
                  town: getUser.body.address.town,
                  postalCode: getUser.body.address.postalCode,
                  country: getUser.body.address.country,
                },
                sex: getUser.body.sex,
                phone: getUser.body.phone,
              },
            }, (err3, user3) => {
              if (err3) {
                const error = {
                  success: false,
                  error: 'Could not update this user',
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const success = {
                  success: true,
                  message: 'User updated with success',
                  user3,
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(success), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          }
        });
      } else if (user.email !== getUser.body.email && user.username !== getUser.body.username) {
        User.findOne({
          $or: [{
            username: getUser.body.username,
          }, {
            email: getUser.body.email,
          }],
        }, (err2, user2) => {
          if (user2) {
            if (user.username === getUser.body.username) {
              const error = {
                success: false,
                error: 'Username already use.',
              };
              channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
                correlationId: msg.properties.correlationId,
              });
            } else {
              const error = {
                success: false,
                error: 'Email already use.',
              };
              channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
                correlationId: msg.properties.correlationId,
              });
            }
          } else {
            User.update({
              _id: getUser.id,
            }, {
              $set: {
                username: getUser.body.username,
                email: getUser.body.email,
                lastname: getUser.body.lastname,
                firstname: getUser.body.firstname,
                address: {
                  street: getUser.body.address.street,
                  number: getUser.body.address.number,
                  town: getUser.body.address.town,
                  postalCode: getUser.body.address.postalCode,
                  country: getUser.body.address.country,
                },
                sex: getUser.body.sex,
                phone: getUser.body.phone,
              },
            }, (err3, user3) => {
              if (err3) {
                const error = {
                  success: false,
                  error: 'Could not update this user',
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const success = {
                  success: true,
                  message: 'User updated with success',
                  user3,
                };
                channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(success), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          }
        });
      } else {
        const error = {
          success: false,
          error: 'Could not update this user',
        };
        channel.publish(msg.properties.replyTo, 'updateUser', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  updatePass: (channel, msg) => {
    console.log('tag => updatePass');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    User.findById(getUser.id, (err, user) => {
      if (err) throw err;
      user.comparePassword(getUser.body.password, (err2, isMatch) => {
        if (err2) throw err2;
        if (isMatch) {
          const password = getUser.body.newPassword;
          bcrypt.genSalt(Salt, (err3, salt) => {
            bcrypt.hash(password, salt, (err4, hash) => {
              user.password = hash;
              user.save();
              const success = {
                success: true,
                message: 'Password updated with success',
              };
              channel.publish(msg.properties.replyTo, 'updatePass', getBuffer(success), {
                correlationId: msg.properties.correlationId,
              });
            });
          });
        } else {
          const error = {
            success: false,
            error: 'Bad password',
          };
          channel.publish(msg.properties.replyTo, 'updatePass', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        }
      });
    });
    channel.ack(msg);
  },
  makeAdmin: (channel, msg) => {
    console.log('tag => makeAdmin');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    if (!getUser.body.id) {
      const error = {
        success: false,
        error: 'No uid provided',
      };
      channel.publish(msg.properties.replyTo, 'makeAdmin', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    } else {
      User.findById(getUser.body.id, (err, user) => {
        if (err) {
          const error = {
            success: false,
            error: 'MongoDB error',
          };
          channel.publish(msg.properties.replyTo, 'makeAdmin', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          user.role = 'admin';
          user.save();
          const success = {
            success: true,
            message: 'Rank updated with success',
          };
          channel.publish(msg.properties.replyTo, 'makeAdmin', getBuffer(success), {
            correlationId: msg.properties.correlationId,
          });
        }
      });
    }
    channel.ack(msg);
  },
  removeAdmin: (channel, msg) => {
    console.log('tag => removeAdmin');
    const getUser = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    if (!getUser.body.id) {
      const error = {
        success: false,
        error: 'No uid provided',
      };
      channel.publish(msg.properties.replyTo, 'removeAdmin', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    } else {
      User.findById(getUser.body.id, (err, user) => {
        if (err) {
          const error = {
            success: false,
            error: 'MongoDB error',
          };
          channel.publish(msg.properties.replyTo, 'removeAdmin', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else if (user.username !== 'admin') {
          user.role = 'user';
          user.save();
          const success = {
            success: true,
            message: 'Rank updated with success',
          };
          channel.publish(msg.properties.replyTo, 'removeAdmin', getBuffer(success), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          const error = {
            success: false,
            error: 'An unexpect error happened',
          };
          channel.publish(msg.properties.replyTo, 'removeAdmin', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        }
      });
    }
    channel.ack(msg);
  },
};

const handleMessage = channel => (q) => {
  handleTags[q.fields.routingKey](channel, q);
};

const createServer = (name, tags) => (err, channel) => {
  channel.prefetch(1);
  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.assertQueue(name, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    tags.forEach((tag) => {
      channel.bindQueue(queue, name, tag);
    });
    channel.consume(queue, handleMessage(channel));
  }, {
    noAck: true,
  });
};

module.exports = {
  createServer,
};
