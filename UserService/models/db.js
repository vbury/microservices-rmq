import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import User from './user';
import { config } from '../tools/tools';

const Salt = config.salt;

mongoose.Promise = global.Promise;

mongoose.connect(config.uri);

mongoose.connection.on('connected', () => {
  console.log(`[Mongoose] connected to ${config.uri}`);
  User.findOne({ username: 'admin' }, (err, user) => {
    if (!user) {
      bcrypt.genSalt(Salt, (err, salt) => {
        bcrypt.hash(config.admin.password, salt, (err, hash) => {
          const admin = User(config.admin);
          admin.password = hash;
          admin.save((err, user) => {
            if (err) {
              console.log(err);
            }
            if (user) {
              console.log(user);
            }
          });
        });
      });
    }
  });
});

mongoose.connection.on('error', (err) => {
  console.log(`[Mongoose] error: ${err}`);
});

const shutdown = (callback) => {
  mongoose.connection.close(() => {
    console.log(`[Mongoose] disconnected from ${config.uri}`);
    callback();
  });
};

process.once('SIGUSR2', () => {
  shutdown(() => {
    process.kill(process.pid, 'SIGUSR2');
  });
});

process.on('SIGINT', () => {
  shutdown(() => {
    process.exit(0);
  });
});

process.on('SIGTERM', () => {
  shutdown(() => {
    process.exit(0);
  });
});

module.exports = {
  Salt,
  User,
};
