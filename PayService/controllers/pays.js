import Paypal from 'paypal-express-checkout';
import {
    InfoPaypal,
    Pay,
    Transaction,
} from '../models/db';
import { config } from '../tools/tools';

const getBuffer = object => Buffer.from(JSON.stringify(object));

const handleTags = {
  pay: (channel, msg) => {
    console.log('tag => pay');
    const pays = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const transaction = Transaction({
      amount: pays.body.price,
      cart: pays.body.cart,
      userId: pays.id,
    });
    transaction.save((err) => {
      if (err) {
        console.log(err);
        const error = {
          success: false,
          error: 'Could not create this transaction',
        };
        channel.publish(msg.properties.replyTo, 'pay', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const paypal = Paypal.init(InfoPaypal.username, InfoPaypal.password, InfoPaypal.signature, `http://${config.client}:5000/api/v1/pay/valid/${transaction.id}`, `http://${config.client}:5000/api/v1/pay/valid/${transaction.id}`, true);
        paypal.pay(transaction.id, transaction.amount, pays.body.desc, 'EUR', true, (err, url) => {
          if (err) {
            console.log(err);
            const error = {
              success: false,
              message: 'Payement failed.',
            };
            channel.publish(msg.properties.replyTo, 'pay', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const success = {
              success: true,
              url,
            };
            channel.publish(msg.properties.replyTo, 'pay', getBuffer(success), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      }
    });
    channel.ack(msg);
  },
  valid: (channel, msg) => {
    console.log('tag => valid');
    const pays = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    if (pays.id) {
      const query = pays;
      if (query.PayerID && query.token) {
        const paypal = Paypal.init(InfoPaypal.username, InfoPaypal.password, InfoPaypal.signature, `http://${config.client}:3000/`, `http://${config.client}:3000/`, true);
        paypal.detail(query.token, query.PayerID, (err, data) => {
          if (err) {
            console.log(err);
            const error = {
              success: false,
              error: 'Could not finish this transaction',
            };
            channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else if (data.success) {
            Transaction.findOne({
              _id: pays.id,
            }, (err2, transaction) => {
              if (err2) {
                console.log(err2);
                const error = {
                  success: false,
                  error: 'Could not finish this transaction',
                };
                channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else if (transaction !== undefined) {
                const pay = Pay({
                  amount: transaction.amount,
                  cart: transaction.cart,
                  userId: transaction.userId,
                  payerId: query.PayerID,
                  token: query.token,
                });
                pay.save((err3, pay2) => {
                  if (err3) {
                    console.log(err3);
                    const error = {
                      success: false,
                      error: 'Could not finish this transaction',
                    };
                    channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
                      correlationId: msg.properties.correlationId,
                    });
                  } else {
                    const success = {
                      success: true,
                      id: pay2.id,
                      message: 'transaction finish',
                    };
                    channel.publish(msg.properties.replyTo, 'valid', getBuffer(success), {
                      correlationId: msg.properties.correlationId,
                    });
                    transaction.remove();
                  }
                });
              } else {
                const error = {
                  success: false,
                  error: 'Could not finish this transaction',
                };
                channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          } else {
            Transaction.findOneAndRemove({
              _id: pays.id,
            }, (err2) => {
              if (err2) {
                const error = {
                  success: false,
                  error: 'Could not finish this transaction',
                };
                channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const error = {
                  success: false,
                  error: 'Could not finish this transaction',
                };
                channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              }
            });
          }
        });
      } else if (query.token) {
        Transaction.findOneAndRemove({
          _id: pays.id,
        }, (err) => {
          if (err) {
            console.log(err);
            const error = {
              success: false,
              error: 'Could not finish this transaction',
            };
            channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const error = {
              success: false,
              error: 'Could not finish this transaction',
            };
            channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      } else {
        const error = {
          success: false,
          error: 'Could not finish this transaction',
        };
        channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
    } else {
      const error = {
        success: false,
        error: 'Could not finish this transaction',
      };
      channel.publish(msg.properties.replyTo, 'valid', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    }
    channel.ack(msg);
  },
  getByUserPayement: (channel, msg) => {
    console.log('tag => getByUandE');
    const pays = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Pay.find({
      userId: pays.id,
    }, (err, pay) => {
      if (!pay) {
        const error = {
          success: false,
          err: 'No payement found :(',
        };
        channel.publish(msg.properties.replyTo, 'getByUserPayement', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getByUserPayement', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const allPay = [];
        pay.forEach((p) => {
          const ByPay = {
            _id: p.id,
            cart: p.cart,
          };
          allPay.push(ByPay);
        });
        const success = {
          success: true,
          allPay,
        };
        channel.publish(msg.properties.replyTo, 'getByUserPayement', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getAllPayement: (channel, msg) => {
    console.log('tag => getAllPayement');
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Pay.find((err, pay) => {
      if (!pay) {
        const error = {
          success: false,
          err: 'No payement found :(',
        };
        channel.publish(msg.properties.replyTo, 'getAllPayement', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getAllPayement', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const allPay = [];
        pay.forEach((p) => {
          const ByPay = {
            _id: p.id,
            cart: p.cart,
          };
          allPay.push(ByPay);
        });
        const success = {
          success: true,
          allPay,
        };
        channel.publish(msg.properties.replyTo, 'getAllPayement', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getById: (channel, msg) => {
    console.log('tag => getById');
    const pays = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Pay.findOne({
      _id: pays.id,
    }, (err, pay) => {
      if (!pay) {
        const error = {
          success: false,
          err: 'No payement found :(',
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          pay,
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
};

const handleMessage = channel => (q) => {
  handleTags[q.fields.routingKey](channel, q);
};

const createServer = (name, tags) => (err, channel) => {
  channel.prefetch(1);
  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.assertQueue(name, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    tags.forEach((tag) => {
      channel.bindQueue(queue, name, tag);
    });
    channel.consume(queue, handleMessage(channel));
  });
};

module.exports = {
  createServer,
};
