import amqp from 'amqplib/callback_api';
import {
    createServer,
} from './controllers/pays';
import { config } from './tools/tools';

const NAME = process.env.NAME || 'Pay API';

const handleConnection = (conn) => {
  conn.createChannel(createServer('pay', ['pay', 'valid', 'getByUserPayement', 'getAllPayement', 'getById']));
};

const start = () => {
  amqp.connect(`${config.rabbitmq}?heartbeat=60`, (err, conn) => {
    if (err) {
      console.error('[AMQP]', err.message);
      return setTimeout(start, 1000);
    }
    conn.on('error', (err) => {
      if (err.message !== 'Connection closing') {
        console.error('[AMQP] conn error', err.message);
      }
    });
    conn.on('close', () => {
      console.error('[AMQP] reconnecting');
      return setTimeout(start, 1000);
    });
    console.log('[AMQP] connected');
    console.log(`${NAME} up and running`);
    handleConnection(conn);
  });
};

start();
