import dev from '../config/dev.json';
import prod from '../config/prod.json';

const config = process.env.DEBUG == 0 ? prod : dev;

const generateUuid = () => Math.random().toString() +
        Math.random().toString() +
        Math.random().toString();

module.exports = {
  config,
  generateUuid,
};
