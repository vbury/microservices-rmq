import mongoose from 'mongoose';
import Transaction from './transaction';
import Pay from './pay';
import { config } from '../tools/tools';

mongoose.Promise = global.Promise;

mongoose.connect(config.uri);

mongoose.connection.on('connected', () => {
  console.log(`[Mongoose] connected to ${config.uri}`);
});

mongoose.connection.on('error', (err) => {
  console.log(`[Mongoose] error: ${err}`);
});

const shutdown = (callback) => {
  mongoose.connection.close(() => {
    console.log(`[Mongoose] disconnected from ${config.uri}`);
    callback();
  });
};

process.once('SIGUSR2', () => {
  shutdown(() => {
    process.kill(process.pid, 'SIGUSR2');
  });
});

process.on('SIGINT', () => {
  shutdown(() => {
    process.exit(0);
  });
});

process.on('SIGTERM', () => {
  shutdown(() => {
    process.exit(0);
  });
});

const InfoPaypal = config.paypal;

module.exports = {
  InfoPaypal,
  Pay,
  Transaction,
};
