import amqp from 'amqplib/callback_api';
import { config, handleError, createClient } from '../tools/tools';

const PORT = process.env.PORT || 5000;

module.exports.getAll = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {},
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('cats', 'getAll', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.categories);
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getAllWithDetails = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {},
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('cats', 'getAllWithDetails', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response);
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.addCat = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('cats', 'addCat', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(201);
      res.json({
        success: true,
        link: `http://localhost:${PORT}/api/v1/cats/${response.cat.nom}`,
      });
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getByCat = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        cat: req.params.cat,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('cats', 'getByCat', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.products);
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.deleteCatCascade = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('cats', 'deleteCatCascade', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(204);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};
