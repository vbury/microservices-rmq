import amqp from 'amqplib/callback_api';
import { config, handleError, createClient } from '../tools/tools';

module.exports.pay = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
        body: req.body,
      },
      response,
      error,
      time: 2000,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('pay', 'pay', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json({
        success: response.success,
        url: response.url,
      });
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch(() => {
    if (res.headersSent === false) {
      res.status(500);
      res.redirect(`http://${config.client}:3000/checkout/error`);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.valid = (req, res) => {
  const query = req.query;
  if (query.token && query.PayerID) {
    const handleConnection = (err, conn) => new Promise((response, error) => {
      const object = {
        request: {
          id: req.params.id,
          token: query.token,
          PayerID: query.PayerID,
        },
        response,
        error,
        time: 5000,
      };
      handleError(err, res);
      if (!err) {
        conn.createChannel(createClient('pay', 'valid', object));
      }
    }).then((response) => {
      if (response.success) {
        res.status(200);
        res.redirect(`http://${config.client}:3000/checkout/success/${response.id}`);
      } else {
        res.status(500);
        res.redirect('http://${config.client}:3000/checkout/error');
      }
      conn.close();
    }).catch(() => {
      if (res.headersSent === false) {
        res.status(500);
        res.redirect(`http://${config.client}:3000/checkout/error`);
        conn.close();
      }
    });
    amqp.connect(config.rabbitmq, handleConnection);
  } else if (query.token) {
    const handleConnection = (err, conn) => new Promise((response, error) => {
      const object = {
        request: {
          id: req.params.id,
          token: query.token,
        },
        response,
        error,
      };
      handleError(err, res);
      if (!err) {
        conn.createChannel(createClient('pay', 'valid', object));
      }
    }).then((response) => {
      if (response.success) {
        res.status(200);
        res.redirect(`http://${config.client}:3000/checkout/success/${response.id}`);
      } else {
        res.status(500);
        res.redirect(`http://${config.client}:3000/checkout/error`);
      }
      conn.close();
    }).catch(() => {
      if (res.headersSent === false) {
        res.status(500);
        res.redirect(`http://${config.client}:3000/checkout/error`);
        conn.close();
      }
    });
    amqp.connect(config.rabbitmq, handleConnection);
  } else {
    res.status(204);
    res.redirect(`http://${config.client}:3000/checkout/error`);
  }
};

module.exports.getById = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.query.transid,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('pay', 'getById', object));
    }
  }).then((response) => {
    if (response.success) {
      req.body = response;
      next();
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};


module.exports.getByUserPayement = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('pay', 'getByUserPayement', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(202);
      res.json(response.allPay);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getAllPayement = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {},
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('pay', 'getAllPayement', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(202);
      res.json(response.allPay);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};
