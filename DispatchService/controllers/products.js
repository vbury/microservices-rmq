import amqp from 'amqplib/callback_api';
import { config, handleError, createClient } from '../tools/tools';

const PORT = process.env.PORT || 5000;

module.exports.getAll = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {},
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'getAll', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.products);
    } else {
      res.status(404);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getById = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'getById', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.products);
    } else {
      res.status(404);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.addProduct = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'addProduct', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.send({
        id: response.id,
        link: `http://${config.client}:${PORT}/api/v1/products/${response.id}`,
      });
    } else {
      res.status(404);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.delProduct = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'delProduct', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(204);
      res.json(response);
    } else {
      res.status(404);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.updateProduct = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'updateProduct', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(204);
      res.json({
        message: response.message,
        product: response.products,
      });
    } else {
      res.status(404);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.search = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        query: req.params.query,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'search', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getProductCart = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'getProductCart', object));
    }
  }).then((response) => {
    if (response.success) {
      req.body = response;
      next();
    } else {
      res.redirect(`http://${config.client}:3000/checkout/error`);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getProductPay = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('products', 'getProductPay', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(202);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
}
;
