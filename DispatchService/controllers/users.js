import amqp from 'amqplib/callback_api';
import {
  config,
  handleError,
  createClient,
} from '../tools/tools';

module.exports.getAll = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {},
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'getAll', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.allUsers);
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.getById = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'getById', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response.ByUser);
    } else {
      res.status(400);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};
module.exports.getByUandE = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        username: req.body.username,
        password: req.body.password,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'getByUandE', object));
    }
  }).then((response) => {
    if (response.success) {
      req.body = response;
      next();
    } else {
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.addUser = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'addUser', object));
    }
  }).then((response) => {
    if (response.success) {
      req.body = response;
      next();
    } else {
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.delUser = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'delUser', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(204);
      res.end();
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.updateUser = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'updateUser', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json({
        user: response.user,
        message: response.message,
      });
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.updatePass = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        id: req.params.id,
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'updatePass', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(204);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.makeAdmin = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'makeAdmin', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.removeAdmin = (req, res) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        body: req.body,
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('user', 'removeAdmin', object));
    }
  }).then((response) => {
    if (response.success) {
      res.status(200);
      res.json(response);
    } else {
      res.status(500);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};
