import amqp from 'amqplib/callback_api';
import {
    config,
    handleError,
    createClient,
} from '../tools/tools';

module.exports.auth = (req, res) => {
  if (req.body.username !== '' && req.body.password !== '') {
    const handleConnection = (err, conn) => new Promise((response, error) => {
      const object = {
        request: {
          body: req.body,
        },
        response,
        error,
      };
      handleError(err, res);
      if (!err) {
        conn.createChannel(createClient('auth', 'auth', object));
      }
    }).then((response) => {
      if (response.success) {
        res.status(200);
        res.json(response);
      } else {
        res.status(401);
        res.json(response);
      }
      conn.close();
    }).catch((error) => {
      if (res.headersSent === false) {
        handleError(error, res);
        conn.close();
      }
    });
    amqp.connect(config.rabbitmq, handleConnection);
  } else {
    res.status(401);
    res.json({
      sucess: false,
      message: 'Failed to Authentication token.',
    });
  }
};


module.exports.requireId = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    console.log(req.params.id);
    const object = {
      request: {
        id: req.params.id,
        token: req.headers['x-access-token'],
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('auth', 'requireId', object));
    }
  }).then((response) => {
    if (response.success) {
      next();
    } else {
      res.status(401);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};

module.exports.requireAdmin = (req, res, next) => {
  const handleConnection = (err, conn) => new Promise((response, error) => {
    const object = {
      request: {
        token: req.headers['x-access-token'],
      },
      response,
      error,
    };
    handleError(err, res);
    if (!err) {
      conn.createChannel(createClient('auth', 'requireAdmin', object));
    }
  }).then((response) => {
    if (response.success) {
      next();
    } else {
      res.status(401);
      res.json(response);
    }
    conn.close();
  }).catch((error) => {
    if (res.headersSent === false) {
      handleError(error, res);
      conn.close();
    }
  });
  amqp.connect(config.rabbitmq, handleConnection);
};
