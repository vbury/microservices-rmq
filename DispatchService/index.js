import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import users from './routes/users';
import auth from './routes/auth';
import products from './routes/products';
import cats from './routes/cats';
import pay from './routes/pay';
import err404 from './routes/err404';

const app = express();

const PORT = process.env.PORT || 5000;
const NAME = process.env.NAME || 'e-commerce API';

app.use(logger('dev'));
app.use(bodyParser.json());

app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).json({
    error: 'Something broke',
  });
  next();
});

app.disable('x-powered-by');
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
  next();
});

app.use('/api/v1/users', users);
app.use('/api/v1/auth', auth);
app.use('/api/v1/products', products);
app.use('/api/v1/cats', cats);
app.use('/api/v1/pay', pay);
app.use('*', err404);

app.listen(PORT, () => {
  console.log(`${NAME} up and running on port ${PORT} 🎉`);
});
