import express from 'express'
import {
    auth,
    requireId,
    requireAdmin
} from '../controllers/auth'
import {
    makeAdmin,
    removeAdmin,
    getAll,
    getById,
    addUser,
    delUser,
    updateUser,
    updatePass
} from '../controllers/users'

const router = express.Router()

router.post('/admins', requireAdmin, makeAdmin)
router.delete('/admins', requireAdmin, removeAdmin)

router.get('/', requireAdmin, getAll)
router.get('/:id', requireId, getById)
router.post('/', addUser, auth)
router.delete('/:id', requireId, delUser)
router.put('/:id', requireId, updateUser)
router.post('/pass/:id', requireId, updatePass)


module.exports = router