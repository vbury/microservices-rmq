import express from 'express'
import {
    requireId,
    requireAdmin
} from '../controllers/auth'
import {
    getProductCart,
    getProductPay
} from '../controllers/products'
import {
    pay,
    valid,
    getById,
    getByUserPayement,
    getAllPayement
} from '../controllers/pay'

const router = express.Router()

router.post('/:id', requireId, getProductCart, pay)
router.get('/valid/:id', valid)
router.get('/payements/:id', requireId, getById, getProductPay)
router.get('/payements/user/:id', requireId, getByUserPayement)
router.get('/payements/', requireAdmin, getAllPayement)

module.exports = router
