import express from 'express'
import {
    requireAdmin
} from '../controllers/auth'
import {
    getAll,
    getById,
    addProduct,
    delProduct,
    updateProduct,
    search
} from '../controllers/products'

const router = express.Router()

router.get('/', getAll)
router.get('/:id', getById)
router.post('/', requireAdmin, addProduct)
router.delete('/:id', requireAdmin, delProduct)
router.put('/:id', requireAdmin, updateProduct)
router.get('/search/:query', search)

module.exports = router
