import express from 'express'
import {
    requireAdmin
} from '../controllers/auth'
import {
    getAll,
    getAllWithDetails,
    getByCat,
    addCat,
    deleteCatCascade
} from '../controllers/cats'

const router = express.Router()

router.get('/', getAll)
router.get('/details', getAllWithDetails)
router.get('/:cat', getByCat)
router.post('/', requireAdmin, addCat)
router.delete('/:id', requireAdmin,  deleteCatCascade)

module.exports = router