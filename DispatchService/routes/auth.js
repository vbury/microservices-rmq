import express from 'express'
import { auth }  from '../controllers/auth'
import { getByUandE } from '../controllers/users'

const router = express.Router()

router.post('/', getByUandE, auth)


module.exports = router
