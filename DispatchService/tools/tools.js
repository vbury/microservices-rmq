import dev from '../config/dev.json';
import prod from '../config/prod.json';

const config = process.env.DEBUG == 0 ? prod : dev;

const generateUuid = () => Math.random().toString() +
  Math.random().toString() +
  Math.random().toString();

const getBuffer = object => Buffer.from(JSON.stringify(object));

const handleError = (error, res) => {
  if (error) {
    console.log(error);
    res.status(500);
    res.json({
      sucess: false,
      message: 'An unexpect error happened',
    });
  }
};

const createClient = (name, tag, object) => (err, channel) => {
  console.log(`tag => ${tag} => ${name}`);
  const correlationId = generateUuid();
  const replyTo = `${name}Cli`;
  const timeout = object.time || 1000;

  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.publish(name, tag, getBuffer(object.request), {
    correlationId,
    replyTo,
  });
  channel.assertExchange(replyTo, 'direct', {
    durable: false,
  });
  channel.assertQueue(replyTo, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    channel.bindQueue(queue, replyTo, tag);
    channel.consume(queue, (msg) => {
      if (correlationId === msg.properties.correlationId) {
        const message = JSON.parse(msg.content);
        object.response(message);
      }
    }, {
      noAck: true,
    });
  });
  setTimeout(() => {
    object.error(`No response from ${name}`);
  }, timeout);
};

module.exports = {
  config,
  generateUuid,
  handleError,
  createClient,
};
