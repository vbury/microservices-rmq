import jwt from 'jsonwebtoken';
import { config } from '../tools/tools';

const getBuffer = object => Buffer.from(JSON.stringify(object));

const genJWT = (user, callback) => {
  const userFields = {
    _id: user._id,
    lastname: user.lastname,
    firstname: user.firstname,
    username: user.username,
    email: user.email,
    sex: user.sex,
    phone: user.phone,
    address: {
      street: user.address.street,
      number: user.address.number,
      town: user.address.town,
      postalCode: user.address.postalCode,
      country: user.address.country,
    },
    role: user.role,
  };

  const token = jwt.sign(userFields, config.secret, {
    expiresIn: '72h',
  });

  callback(token);
};

// Serveur

const handleTags = {
  auth: (channel, msg) => {
    console.log('tag => auth');
    const ask = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    if (ask.body.success) {
      genJWT(ask.body.ByUser, (token) => {
        const success = {
          success: true,
          message: 'Enjoy your token!',
          token,
        };
        channel.publish(msg.properties.replyTo, 'auth', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      });
    } else {
      const error = {
        success: false,
        message: 'Failed to Authentication token.',
      };
      channel.publish(msg.properties.replyTo, 'auth', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    }
    channel.ack(msg);
  },
  requireId: (channel, msg) => {
    console.log('tag => requireId');
    const ask = JSON.parse(msg.content);
    console.log(ask);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const token = ask.token;
    if (token) {
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          const error = {
            sucess: false,
            message: 'Failed to Authentication token.',
          };

          channel.publish(msg.properties.replyTo, 'requireId', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else if ((decoded && decoded._id === ask.id) || (decoded.role === 'admin')) {
          const success = {
            success: true,
            message: `Welcome ${decoded.username}`,
          };
          console.log(success);
          channel.publish(msg.properties.replyTo, 'requireId', getBuffer(success), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          const error = {
            sucess: false,
            message: 'Failed to Authentication token.',
          };

          channel.publish(msg.properties.replyTo, 'requireId', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        }
      });
    } else {
      const error = {
        sucess: false,
        message: 'No token provided.',
      };

      channel.publish(msg.properties.replyTo, 'requireId', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    }
    channel.ack(msg);
  },
  requireAdmin: (channel, msg) => {
    console.log('tag => requireAdmin');
    const ask = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const token = ask.token;
    if (token) {
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          const error = {
            sucess: false,
            message: 'Failed to Authentication token.',
          };

          channel.publish(msg.properties.replyTo, 'requireAdmin', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else if (!decoded || decoded.role !== 'admin') {
          const error = {
            sucess: false,
            message: 'This route can only be accessed by an administrator',
          };

          channel.publish(msg.properties.replyTo, 'requireAdmin', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          const success = {
            success: true,
            message: `Welcome ${decoded.username}`,
          };
          channel.publish(msg.properties.replyTo, 'requireAdmin', getBuffer(success), {
            correlationId: msg.properties.correlationId,
          });
        }
      });
    } else {
      const error = {
        sucess: false,
        message: 'No token provided.',
      };

      channel.publish(msg.properties.replyTo, 'requireAdmin', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    }
    channel.ack(msg);
  },
};

const handleMessage = channel => (q) => {
  handleTags[q.fields.routingKey](channel, q);
};

const createServer = (name, tags) => (err, channel) => {
  channel.prefetch(1);
  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.assertQueue(name, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    tags.forEach((tag) => {
      channel.bindQueue(queue, name, tag);
    });
    channel.consume(queue, handleMessage(channel));
  });
};

module.exports = {
  createServer,
};
