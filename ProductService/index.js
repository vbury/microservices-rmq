import amqp from 'amqplib/callback_api';
import { catsServer } from './controllers/cats';
import { productsServer } from './controllers/products';
import { config } from './tools/tools';

const NAME = process.env.NAME || 'Products API';

const handleConnection = (conn) => {
  conn.createChannel(catsServer('cats', ['getAll', 'getAllWithDetails', 'addCat', 'getByCat', 'deleteCatCascade']));
  conn.createChannel(productsServer('products', ['getAll', 'getById', 'addProduct', 'delProduct', 'updateProduct', 'search', 'getProductCart', 'getProductPay']));
};

const start = () => {
  amqp.connect(`${config.rabbitmq}?heartbeat=60`, (err, conn) => {
    if (err) {
      console.error('[AMQP]', err.message);
      return setTimeout(start, 1000);
    }
    conn.on('error', (err) => {
      if (err.message !== 'Connection closing') {
        console.error('[AMQP] conn error', err.message);
      }
    });
    conn.on('close', () => {
      console.error('[AMQP] reconnecting');
      return setTimeout(start, 1000);
    });
    console.log('[AMQP] connected');
    console.log(`${NAME} up and running`);
    handleConnection(conn);
  });
};

start();
