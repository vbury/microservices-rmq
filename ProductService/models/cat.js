import mongoose from 'mongoose';

const catSchema = new mongoose.Schema({
  nom: {
    type: String,
    required: true,
    unique: true,
  },
});

module.exports = mongoose.model('Cat', catSchema);
