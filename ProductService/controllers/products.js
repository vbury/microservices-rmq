import {
    Product,
} from '../models/db';

const getBuffer = object => Buffer.from(JSON.stringify(object));

const handleTags = {
  getAll: (channel, msg) => {
    console.log('tag => getAll');
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.find((err, products) => {
      if (!products) {
        const error = {
          success: false,
          err: 'No products found :(',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpect error happened',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          products,
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getById: (channel, msg) => {
    console.log('tag => getById');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.findById(product.id, (err, products) => {
      if (products) {
        const success = {
          success: true,
          products,
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const error = {
          success: false,
          error: 'No product found for that id.',
        };
        channel.publish(msg.properties.replyTo, 'getById', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  addProduct: (channel, msg) => {
    console.log('tag => addProduct');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.findOne({
      nom: product.body.nom,
    }, (err, products) => {
      if (products) {
        const error = {
          success: false,
          error: 'Product already use.',
        };
        channel.publish(msg.properties.replyTo, 'addProduct', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        Product(product.body).save((err2, products2) => {
          if (err2) {
            console.log(err2);
            const error = {
              success: false,
              error: 'Could not create this product',
            };
            channel.publish(msg.properties.replyTo, 'addProduct', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const success = {
              success: true,
              id: products2.id,
              products,
            };
            channel.publish(msg.properties.replyTo, 'addProduct', getBuffer(success), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      }
    });
    channel.ack(msg);
  },
  delProduct: (channel, msg) => {
    console.log('tag => delProduct');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.findOneAndRemove({
      _id: product.id,
    }, (err) => {
      if (err) {
        const error = {
          success: false,
          error: 'Could not remove this product :(',
        };
        channel.publish(msg.properties.replyTo, 'delProduct', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          message: 'Product deleted with success',
        };
        channel.publish(msg.properties.replyTo, 'delProduct', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  updateProduct: (channel, msg) => {
    console.log('tag => updateProduct');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.update({
      _id: product.id,
    }, {
      $set: product.body.product,
    }, (err, products) => {
      if (err) {
        const error = {
          success: false,
          error: 'Could not update this product :(',
        };
        channel.publish(msg.properties.replyTo, 'updateProduct', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          message: 'Product updated with success',
          products,
        };
        channel.publish(msg.properties.replyTo, 'updateProduct', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  search: (channel, msg) => {
    console.log('tag => search');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const {
            query,
        } = product;
    Product.search({
      query: {
        multi_match: {
          query,
          fields: ['nom', 'desc', 'cat', 'tag'],
          fuzziness: 2,
        },
      },
    }, {
      from: 0,
      size: 1000,
      hydrate: true,
    }, (err, results) => {
      if (err) {
        const error = {
          success: false,
          error: 'No product found',
        };
        channel.publish(msg.properties.replyTo, 'search', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (results.hits) {
        if (results.hits.total !== 0) {
          const success = {
            success: true,
            products: results.hits.hits,
          };
          channel.publish(msg.properties.replyTo, 'search', getBuffer(success), {
            correlationId: msg.properties.correlationId,
          });
        } else {
          const error = {
            success: true,
            message: 'No product found',
          };
          channel.publish(msg.properties.replyTo, 'search', getBuffer(error), {
            correlationId: msg.properties.correlationId,
          });
        }
      } else {
        const error = {
          success: false,
          error: 'No product found',
        };
        channel.publish(msg.properties.replyTo, 'search', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getProductCart: (channel, msg) => {
    console.log('tag => getProductCart');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const cart = product.body.cart;
    let price = 0;
    let i = 0;
    let desc = '';
    if (cart) {
      if (product.id) {
        cart.forEach((prod) => {
          Product.findById(prod.id, (err, products) => {
            if (products) {
              price += products.price.value * prod.quantity;
              if (i !== cart.length - 1) {
                desc += `${products.nom} - ${products.price.value}€ x${prod.quantity}`;
              } else {
                desc += `${products.nom} - ${products.price.value}€ x${prod.quantity}`;
              }
              i++;
            } else {
              const index = cart.indexOf(prod);
              if (index > -1) {
                cart.splice(index, 1);
              }
            }
            if (i === cart.length) {
              if (price === 0) {
                const error = {
                  success: false,
                  error: 'Could not create this transaction',
                };
                channel.publish(msg.properties.replyTo, 'getProductCart', getBuffer(error), {
                  correlationId: msg.properties.correlationId,
                });
              } else {
                const success = {
                  success: true,
                  price,
                  cart,
                  desc,
                };
                channel.publish(msg.properties.replyTo, 'getProductCart', getBuffer(success), {
                  correlationId: msg.properties.correlationId,
                });
              }
            }
          });
        });
      }
    }
    channel.ack(msg);
  },
  getProductPay: (channel, msg) => {
    console.log('tag => getProductPay');
    const product = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    const cartLength = product.body.pay.cart.length;
    const newCart = [];
    let i = 0;
    if (product.body.pay) {
      for (const item of product.body.pay.cart) {
        Product.findById(item.id, (err, products) => {
          i++;
          if (products) {
            newCart.push({
              product: products,
              quantity: item.quantity,
            });
            if (i == cartLength) {
              const success = {
                success: true,
                cart: newCart,
              };
              channel.publish(msg.properties.replyTo, 'getProductPay', getBuffer(success), {
                correlationId: msg.properties.correlationId,
              });
            }
          } else {
            newCart.push({
              product: {
                nom: 'Produit supprimé',
                price: {
                  value: 0,
                },
              },
              quantity: item.quantity,
            });
            if (i === cartLength) {
              const success = {
                success: true,
                cart: newCart,
              };
              channel.publish(msg.properties.replyTo, 'getProductPay', getBuffer(success), {
                correlationId: msg.properties.correlationId,
              });
            }
          }
        });
      }
    } else {
      const error = {
        success: false,
        error: 'Could not get this transaction',
      };
      channel.publish(msg.properties.replyTo, 'getProductPay', getBuffer(error), {
        correlationId: msg.properties.correlationId,
      });
    }
    channel.ack(msg);
  },
};

const handleMessage = channel => (q) => {
  handleTags[q.fields.routingKey](channel, q);
};

const productsServer = (name, tags) => (err, channel) => {
  channel.prefetch(1);
  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.assertQueue(name, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    tags.forEach((tag) => {
      channel.bindQueue(queue, name, tag);
    });
    channel.consume(queue, handleMessage(channel));
  });
};

module.exports = {
  productsServer,
};
