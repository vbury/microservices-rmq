import {
    Cat,
    Product,
} from '../models/db';

const getBuffer = object => Buffer.from(JSON.stringify(object));

const handleTags = {
  getAll: (channel, msg) => {
    console.log('tag => getAll');
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Cat.find((err, cats) => {
      if (cats.length === 0) {
        const error = {
          success: true,
          err: 'No categories found :(',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpected error happened',
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const categories = [];
        cats.forEach((cat) => {
          categories.push(cat.nom);
        });
        const success = {
          success: true,
          categories,
        };
        channel.publish(msg.properties.replyTo, 'getAll', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  getAllWithDetails: (channel, msg) => {
    console.log('tag => getAllWithDetails');
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Cat.find((err, cats) => {
      if (cats.length === 0) {
        const error = {
          success: true,
          err: 'No categories found :(',
          cats: [],
        };
        channel.publish(msg.properties.replyTo, 'getAllWithDetails', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpected error happened',
        };
        channel.publish(msg.properties.replyTo, 'getAllWithDetails', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          cats,
        };
        channel.publish(msg.properties.replyTo, 'getAllWithDetails', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  addCat: (channel, msg) => {
    console.log('tag => addCat');
    const cats = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Cat.findOne({
      nom: cats.body.nom,
    }, (err, cat) => {
      if (err) {
        const error = {
          success: false,
          error: 'An unexpected error happened',
        };
        channel.publish(msg.properties.replyTo, 'addCat', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (cat) {
        const error = {
          success: false,
          error: 'Category already use',
        };
        channel.publish(msg.properties.replyTo, 'addCat', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        Cat(cats.body).save((err2, cat2) => {
          if (err2) {
            console.log(err2);
            const error = {
              success: false,
              error: 'Could not create this category',
            };
            channel.publish(msg.properties.replyTo, 'addCat', getBuffer(error), {
              correlationId: msg.properties.correlationId,
            });
          } else {
            const success = {
              success: true,
              cat2,
            };
            channel.publish(msg.properties.replyTo, 'addCat', getBuffer(success), {
              correlationId: msg.properties.correlationId,
            });
          }
        });
      }
    });
    channel.ack(msg);
  },
  getByCat: (channel, msg) => {
    console.log('tag => getByCat');
    const cats = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Product.find({
      cat: new RegExp(cats.cat, 'i'),
    }, (err, products) => {
      if (products.length === 0) {
        const error = {
          success: true,
          err: 'No products found in this category :(',
          products: [],
        };
        channel.publish(msg.properties.replyTo, 'getByCat', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else if (err) {
        const error = {
          success: false,
          err: 'An unexpected error happened',
        };
        channel.publish(msg.properties.replyTo, 'getByCat', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          products,
        };
        channel.publish(msg.properties.replyTo, 'getByCat', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
  deleteCatCascade: (channel, msg) => {
    console.log('tag => deleteCatCascade');
    const cats = JSON.parse(msg.content);
    channel.assertExchange(msg.properties.replyTo, 'direct', {
      durable: false,
    });
    Cat.findOneAndRemove({
      _id: cats.id,
    }, (err) => {
      if (err) {
        const error = {
          success: false,
          err: 'An unexpected error happened',
        };
        channel.publish(msg.properties.replyTo, 'deleteCatCascade', getBuffer(error), {
          correlationId: msg.properties.correlationId,
        });
      } else {
        const success = {
          success: true,
          message: 'category deleted with success',
        };
        channel.publish(msg.properties.replyTo, 'deleteCatCascade', getBuffer(success), {
          correlationId: msg.properties.correlationId,
        });
      }
    });
    channel.ack(msg);
  },
};

const handleMessage = channel => (q) => {
  handleTags[q.fields.routingKey](channel, q);
};

const catsServer = (name, tags) => (err, channel) => {
  channel.prefetch(1);
  channel.assertExchange(name, 'direct', {
    durable: false,
  });
  channel.assertQueue(name, {
    durable: false,
  }, (err2, {
            queue,
        }) => {
    tags.forEach((tag) => {
      channel.bindQueue(queue, name, tag);
    });
    channel.consume(queue, handleMessage(channel));
  });
};

module.exports = {
  catsServer,
};
